package com.capgemini.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.entity.DepartmentEntity;
import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.service.ContractService;
import com.capgemini.service.DepartmentService;
import com.capgemini.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@AutoConfigureDataJpa
public class DepartmentServiceImplTest {

	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private ContractService contractService;
	@Autowired
	private EmployeeService employeeService;

	@Test
	public void shouldAddDepartment() {
		// given
		DepartmentEntity d = departmentService.findOne(1L);
		int departmentSizeBeforeAdd = departmentService.findAll().size();
		DepartmentEntity departmentEntity = new DepartmentEntity("555 555 555", d.getAddressEntity(), null);
		// when
		DepartmentEntity added = departmentService.add(departmentEntity);
		// then
		assertEquals("555 555 555", added.getPhoneNumber());
		assertEquals(departmentSizeBeforeAdd + 1, departmentService.findAll().size());
		assertTrue(added.getCreationDate() != null);
	}

	@Test
	public void shouldTestRemoveDepartment() {
		// given
		int departmentSizeBeforeAdd = departmentService.findAll().size();
		int sizeOfContractsBeforeRemove = contractService.findAll().size();
		int sizeOfEmployeesBeforeRemove = employeeService.findAll().size();
		// when
		departmentService.remove(1L);
		// then
		assertEquals(departmentSizeBeforeAdd - 1, departmentService.findAll().size());
		assertEquals(sizeOfEmployeesBeforeRemove, employeeService.findAll().size());
		assertTrue(sizeOfContractsBeforeRemove != contractService.findAll().size());
	}

	@Test
	public void shouldUpdateDepartment() {
		// given
		DepartmentEntity departmentEntity = departmentService.findOne(1L);
		departmentEntity.setPhoneNumber("666 666 666");
		// when
		departmentEntity = departmentService.update(departmentEntity);
		// then
		assertEquals("666 666 666", departmentEntity.getPhoneNumber());
	}

	@Test
	public void shouldAddEmployeeToDepartment() {
		// given
		DepartmentEntity d = departmentService.findOne(1L);
		DepartmentEntity departmentEntity = new DepartmentEntity("555 555 555", d.getAddressEntity(), d.getEmployes());
		EmployeeEntity e = employeeService.findOne(1L);
		EmployeeEntity employeeEntity = new EmployeeEntity(e.getPersonalData(), e.getBirtDate(), e.getProffesion(),
				e.getAddressEntity(), null, null);
		Long departmentIdBeforeAdded = departmentEntity.getId();
		Long employeeIdBeforeAdded = employeeEntity.getId();
		departmentEntity = departmentService.add(departmentEntity);
		employeeEntity = employeeService.add(employeeEntity);
		// when
		departmentService.addEmployeeToDepartment(employeeEntity.getId(), departmentEntity.getId());
		int size = departmentService.findOne(departmentEntity.getId()).getEmployes().size();
		// then
		assertEquals(departmentIdBeforeAdded, null);
		assertTrue(departmentEntity.getId() != null);
		assertEquals(employeeIdBeforeAdded, null);
		assertTrue(employeeEntity.getId() != null);
		assertEquals(employeeEntity.getId(),
				departmentService.findOne(departmentEntity.getId()).getEmployes().get(size - 1).getId());
	}

	@Test
	public void shouldRemoveEmployeeFromDepartment() {
		// given
		DepartmentEntity departmentEntity = departmentService.findOne(1L);
		int size = departmentEntity.getEmployes().size();
		EmployeeEntity employeeEntity = departmentEntity.getEmployes().get(size - 1);
		// when
		departmentService.removeEmployeeFromDepartment(employeeEntity.getId(), departmentEntity.getId());

		// then
		assertEquals(size - 1, departmentService.findOne(1L).getEmployes().size());
		assertEquals(null, employeeService.findOne(employeeEntity.getId()).getDepartmentEntity());
	}

	@Test
	public void shouldGetAllActualEmployeesFromDepartmentById() {
		// given
		DepartmentEntity departmentEntity = departmentService.findOne(1L);
		int size = departmentEntity.getEmployes().size();
		// when
		int sizeOfEmployeeByMethod = departmentService.getEmployeesFromDepartment(1L).size();
		// then
		assertEquals(size, sizeOfEmployeeByMethod);
	}

	@Test
	public void shouldGetEmployeesByDepartmentAndCarsId() {
		// given
		DepartmentEntity departmentEntity = new DepartmentEntity();
		departmentEntity.setId(1L);
		CarEntity car = new CarEntity();
		car.setId(6L);
		// when
		List<EmployeeEntity> employees = departmentService.getAttendantsByDepartmentAndCar(car, departmentEntity);
		// then
		assertEquals(1, employees.size());
		assertEquals("Christian", employees.get(0).getPersonalData().getFirstName());
		//
		// // given
		// DepartmentEntity departmentEntity = departmentService.findOne(1L);
		// CarEntity car = carService.findOne(6L);
		// // when
		// List<EmployeeEntity> employees =
		// departmentService.getAttendantsByDepartmentAndCar(car,
		// departmentEntity);
		// // then
		// assertEquals(1, employees.size());
		// assertEquals("Christian",
		// employees.get(0).getPersonalData().getFirstName());
	}

}
