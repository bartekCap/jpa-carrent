package com.capgemini.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.dataaccess.enums.CarType;
import com.capgemini.service.CarService;
import com.capgemini.service.ContractService;
import com.capgemini.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@AutoConfigureDataJpa
public class CarServiceImplTest {

	@Autowired
	private CarService carService;

	@Autowired
	private ContractService contractService;

	@Autowired
	private EmployeeService employeeService;

	@PersistenceContext
	private EntityManager em;

	@Test
	public void shouldUpdateVersionAfterUpdate() {
		// given
		CarEntity carEntity = new CarEntity(CarType.COMBI, "Volvo", (short) 2000, "Blue", (short) 1998, (short) 120,
				10000);
		// when
		CarEntity addedCar = carService.add(carEntity);
		addedCar.setColor("pink");
		em.detach(addedCar);
		CarEntity updatedCar = carService.update(addedCar);
		em.flush();
		// then
		assertEquals("Volvo", addedCar.getMark());
		assertEquals(CarType.COMBI, addedCar.getCarType());
		assertEquals(null, addedCar.getContracts());
		assertEquals(0, addedCar.getVersion());
		assertEquals(1, updatedCar.getVersion());
	}

	@Test(expected = ObjectOptimisticLockingFailureException.class)
	public void shoulThrowExceptionWhenLocking() {
		// given
		CarEntity car1 = carService.findOne(1L);
		CarEntity car2 = carService.findOne(1L);
		// when
		car1.setColor("red");
		em.detach(car1);
		car2.setColor("orange");
		em.detach(car2);
		// then
		carService.update(car1);
		em.flush();
		carService.update(car2);
	}

	@Test
	public void shouldCheckUpdateDate() {
		// given
		CarEntity carEntity = new CarEntity(CarType.COMBI, "Volvo xc", (short) 2000, "Blue", (short) 1998, (short) 120,
				10000);
		carEntity = carService.add(carEntity);
		em.detach(carEntity);
		LocalDateTime beforeUpdate = carEntity.getUpdateDateTime();
		// when
		carEntity = carService.update(carEntity);
		em.flush();
		// then
		assertTrue(carEntity.getUpdateDateTime() != null);
		assertEquals(beforeUpdate, null);
	}

	@Test
	public void shouldAddCarAndReturnAddedCar() {
		// given
		CarEntity carEntity = new CarEntity(CarType.COMBI, "Opel Astra", (short) 2000, "Blue", (short) 1998,
				(short) 120, 10000);
		int sizeBeforeAdding = carService.findAll().size();
		// when
		CarEntity addedCar = carService.add(carEntity);
		// then
		assertEquals("Opel Astra", addedCar.getMark());
		assertEquals(CarType.COMBI, addedCar.getCarType());
		assertEquals(null, addedCar.getContracts());
		assertEquals(0, addedCar.getVersion());
		assertEquals(sizeBeforeAdding + 1, carService.findAll().size());
	}

	@Test
	public void shouldfidCarByCarTypeAndMark() {
		// given
		CarEntity carEntity1 = new CarEntity(CarType.COMBI, "Opel Astra", (short) 2000, "Blue", (short) 1998,
				(short) 120, 10000);
		CarEntity carEntity2 = new CarEntity(CarType.COMBI, "Opel Astra", (short) 2000, "Blue", (short) 1998,
				(short) 120, 10000);
		CarEntity carEntity3 = new CarEntity(CarType.COMBI, "Opel Astra", (short) 2000, "Blue", (short) 1998,
				(short) 120, 10000);
		carService.add(carEntity1);
		carService.add(carEntity2);
		carService.add(carEntity3);
		// when
		List<CarEntity> cars = carService.getCarsByCarTypeAndMark(CarType.COMBI, "Opel Astra");
		// then
		assertEquals(3, cars.size());
	}

	@Test
	public void shouldAddOneCarAndUpdateThis() {
		// given
		CarEntity carEntity = new CarEntity(CarType.COMBI, "Volvo xc", (short) 2000, "Blue", (short) 1998, (short) 120,
				10000);
		CarEntity addedCar = carService.add(carEntity);
		int size = carService.findAll().size();
		// when
		addedCar.setColor("orange");
		carService.update(addedCar);
		// then
		assertEquals(carService.findAll().size(), size);
	}

	@Test
	public void shouldFindAllCars() {
		// given when
		int size = carService.findAll().size();
		// then
		assertEquals(10, size);
	}

	@Test
	public void shouldRemoveLastCarAndAllContractsWithThisCar() {
		// given
		long sizeOfCars = carService.findAll().size();
		int sizeOfContracts = contractService.findAll().size();
		int sizeOfEmployees = employeeService.findAll().size();
		// when
		carService.removeCarById(sizeOfCars - 1L);
		int sizeOfContractsAfterRemove = contractService.findAll().size();
		long sizeAfterRemove = carService.findAll().size();
		int sizeOfEmployeesAfterRemove = employeeService.findAll().size();
		// then
		assertEquals(sizeOfCars - 1L, sizeAfterRemove);
		assertTrue(sizeOfContracts != sizeOfContractsAfterRemove);
		assertTrue(sizeOfEmployees == sizeOfEmployeesAfterRemove);
	}

	@Test
	public void shouldAddAttendantToCarAndCarToAttendant() {
		// given
		long idEmployee = employeeService.findAll().size() - 1L;
		long idCar = carService.findAll().size() - 1L;
		EmployeeEntity em = employeeService.getEmployee(idEmployee);
		CarEntity car = carService.findOne(idCar);
		int sizeOfAttendants = car.getAttendantEmployee().size();
		int sizeOfCars = em.getAttendCars().size();
		// when
		carService.addAttendantToCar(idCar, idEmployee);
		// then
		assertEquals(sizeOfAttendants + 1, car.getAttendantEmployee().size());
		assertEquals(sizeOfCars + 1, em.getAttendCars().size());
		;
	}

	@Test
	public void shouldGetListOfCarsByAttendants() {
		// given
		long idEmployee = employeeService.findAll().size() - 1L;
		EmployeeEntity em = employeeService.getEmployee(idEmployee);
		// when
		List<CarEntity> cars = carService.getCarsByAttendantId(idEmployee);
		// then
		assertEquals(em.getAttendCars().size(), cars.size());
		assertEquals(em.getAttendCars().get(0), cars.get(0));
	}

	@Test
	public void shouldCheckCreationDate() {
		// given
		CarEntity carEntity = new CarEntity(CarType.COMBI, "Volvo xc", (short) 2000, "Blue", (short) 1998, (short) 120,
				10000);
		LocalDateTime beforeCreation = carEntity.getCreationDate();
		// when
		carEntity = carService.add(carEntity);
		// then
		assertTrue(carEntity.getCreationDate() != null);
		assertEquals(beforeCreation, null);
	}

	@Test
	public void shouldGetCarsRentedByMoreThenTenCustomer() {
		// given when
		List<CarEntity> cars = carService.getCarsRentedByMoreThenTenCustomers();
		// then
		assertEquals(2, cars.size());
		assertEquals((Long) 4L, cars.get(0).getId());
		assertEquals((Long) 9L, cars.get(1).getId());
	}

	@Test
	public void shouldGetCountOfCarsRentedBetweenDates() {
		// given
		LocalDate firstDate = LocalDate.of(2016, 8, 10);
		LocalDate secondDate = LocalDate.of(2016, 8, 11);
		// when
		int count = carService.getCountOfCarsRentedBetwenDates(firstDate, secondDate);
		// then
		assertEquals(7, count);
	}
}
