package com.capgemini.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.dataaccess.enums.Profession;
import com.capgemini.searchcriteria.EmployeeSearchCriteria;
import com.capgemini.service.CarService;
import com.capgemini.service.CriteriaEmployeeService;
import com.capgemini.service.DepartmentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@AutoConfigureDataJpa
public class CriteriaEmployeeServiceImplTest {

	@Autowired
	private CriteriaEmployeeService criteriaEmployeeService;
	@Autowired
	private CarService carService;
	@Autowired
	private DepartmentService departmentService;

	@Test
	public void shouldTestSearchingByThreeCriteriaDepartmentAndCarAndProfession() {
		// given
		EmployeeSearchCriteria employeeSearchCriteria = new EmployeeSearchCriteria();
		employeeSearchCriteria.setDepartment(departmentService.findOne(5L));
		employeeSearchCriteria.setAttendCar(carService.findOne(9L));
		employeeSearchCriteria.setProfession(Profession.ACCOUNTANT);
		// when
		List<EmployeeEntity> employees = criteriaEmployeeService.findEmployeesBySearchCriteria(employeeSearchCriteria);
		// then
		assertEquals(1, employees.size());
		assertEquals("Ingrid", employees.get(0).getPersonalData().getFirstName());
	}

	@Test
	public void shouldTestSearchingByProfession() {
		// given
		EmployeeSearchCriteria employeeSearchCriteria = new EmployeeSearchCriteria();
		employeeSearchCriteria.setProfession(Profession.DEALER);
		// when
		List<EmployeeEntity> employees = criteriaEmployeeService.findEmployeesBySearchCriteria(employeeSearchCriteria);
		// then
		assertEquals(4, employees.size());
		assertEquals("Basil", employees.get(0).getPersonalData().getFirstName());
		assertEquals("Yoshio", employees.get(1).getPersonalData().getFirstName());
		assertEquals("DonoVAN", employees.get(2).getPersonalData().getFirstName());
		assertEquals("Illiana", employees.get(3).getPersonalData().getFirstName());
	}

	@Test
	public void shouldTestSearchingByCar() {
		// given
		EmployeeSearchCriteria employeeSearchCriteria = new EmployeeSearchCriteria();
		employeeSearchCriteria.setAttendCar(carService.findOne(9L));
		// when
		List<EmployeeEntity> employees = criteriaEmployeeService.findEmployeesBySearchCriteria(employeeSearchCriteria);
		// then
		assertEquals(1, employees.size());
		assertEquals("Ingrid", employees.get(0).getPersonalData().getFirstName());
	}

	@Test
	public void shouldTestSearchingByDepartment() {
		// given
		EmployeeSearchCriteria employeeSearchCriteria = new EmployeeSearchCriteria();
		employeeSearchCriteria.setDepartment(departmentService.findOne(5L));
		// when
		List<EmployeeEntity> employees = criteriaEmployeeService.findEmployeesBySearchCriteria(employeeSearchCriteria);
		// then
		assertEquals(2, employees.size());
		assertEquals("Ingrid", employees.get(0).getPersonalData().getFirstName());
		assertEquals("Basil", employees.get(1).getPersonalData().getFirstName());
	}

	@Test
	public void shouldTestSearchingByDepartmentAndProfesion() {
		// given
		EmployeeSearchCriteria employeeSearchCriteria = new EmployeeSearchCriteria();
		employeeSearchCriteria.setDepartment(departmentService.findOne(5L));
		employeeSearchCriteria.setProfession(Profession.DEALER);
		// when
		List<EmployeeEntity> employees = criteriaEmployeeService.findEmployeesBySearchCriteria(employeeSearchCriteria);
		// then
		assertEquals(1, employees.size());
		assertEquals("Basil", employees.get(0).getPersonalData().getFirstName());
	}

	@Test
	public void shouldTestSearchingByCarAndProfesion() {
		// given
		EmployeeSearchCriteria employeeSearchCriteria = new EmployeeSearchCriteria();
		employeeSearchCriteria.setAttendCar(carService.findOne(6L));
		employeeSearchCriteria.setProfession(Profession.MANAGER);
		// when
		List<EmployeeEntity> employees = criteriaEmployeeService.findEmployeesBySearchCriteria(employeeSearchCriteria);
		// then
		assertEquals(1, employees.size());
		assertEquals("Christian", employees.get(0).getPersonalData().getFirstName());
	}

}
