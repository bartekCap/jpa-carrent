package com.capgemini.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.capgemini.service.impl.CarServiceImplTest;
import com.capgemini.service.impl.CriteriaEmployeeServiceImplTest;
import com.capgemini.service.impl.DepartmentServiceImplTest;

@RunWith(Suite.class)
@SuiteClasses({ CarServiceImplTest.class, DepartmentServiceImplTest.class, CriteriaEmployeeServiceImplTest.class })
public class ServiceTestSuite {

}
