package com.capgemini.searchcriteria;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.capgemini.dataaccess.embeded.PersonalData;
import com.capgemini.dataaccess.entity.EmployeeEntity;

@Component
public class EmployeeRowMapper implements RowMapper<EmployeeEntity> {

	@Override
	public EmployeeEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmployeeEntity employeeEntity = new EmployeeEntity();
		employeeEntity.setId(rs.getLong(1));
		employeeEntity.setPersonalData(mapPersonalData(rs.getString(2), rs.getString(3)));
		return employeeEntity;
	}

	private PersonalData mapPersonalData(String firstName, String surname) {
		PersonalData personalData = new PersonalData();
		personalData.setFirstName(firstName);
		personalData.setLastName(surname);
		return personalData;
	}

}
