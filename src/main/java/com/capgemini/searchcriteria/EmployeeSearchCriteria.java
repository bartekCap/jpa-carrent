package com.capgemini.searchcriteria;

import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.entity.DepartmentEntity;
import com.capgemini.dataaccess.enums.Profession;

public class EmployeeSearchCriteria {

	private DepartmentEntity department;
	private CarEntity attendCar;
	private Profession profession;

	public DepartmentEntity getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentEntity department) {
		this.department = department;
	}

	public CarEntity getAttendCar() {
		return attendCar;
	}

	public void setAttendCar(CarEntity attendCar) {
		this.attendCar = attendCar;
	}

	public Profession getProfession() {
		return profession;
	}

	public void setProfession(Profession profession) {
		this.profession = profession;
	}

}
