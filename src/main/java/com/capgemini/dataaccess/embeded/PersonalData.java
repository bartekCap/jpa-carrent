package com.capgemini.dataaccess.embeded;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PersonalData {

	@Column(length = 20, nullable = false)
	private String firstName;
	@Column(length = 30, nullable = false)
	private String surname;
	@Column(length = 15, nullable = false)
	private String phoneNumber;
	@Column(length = 50, nullable = false)
	private String mail;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return surname;
	}

	public void setLastName(String lastName) {
		this.surname = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

}
