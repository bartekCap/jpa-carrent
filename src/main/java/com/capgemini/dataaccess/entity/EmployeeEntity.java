package com.capgemini.dataaccess.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.capgemini.dataaccess.embeded.PersonalData;
import com.capgemini.dataaccess.enums.Profession;

@Entity
@Table(name = "employee")
public class EmployeeEntity extends AbstractEntity {

	@Embedded
	private PersonalData personalData;
	@NotNull
	private LocalDate birthDate;
	@Enumerated(EnumType.STRING)
	@NotNull
	private Profession profession;
	@ManyToOne
	@JoinColumn(name = "id_address")
	private AddressEntity addressEntity;
	@ManyToOne
	@JoinColumn(name = "id_department")
	private DepartmentEntity departmentEntity;
	@ManyToMany
	@JoinTable(name = "attendant", joinColumns = { @JoinColumn(name = "id_employee") }, inverseJoinColumns = {
			@JoinColumn(name = "id_car") })
	private List<CarEntity> attendCars;

	public EmployeeEntity() {
	}

	public EmployeeEntity(PersonalData personalData, LocalDate birthDate, Profession profession,
			AddressEntity addressEntity, DepartmentEntity departmentEntity, List<CarEntity> attendCars) {
		this.personalData = personalData;
		this.birthDate = birthDate;
		this.profession = profession;
		this.addressEntity = addressEntity;
		this.departmentEntity = departmentEntity;
		this.attendCars = attendCars;
	}

	public PersonalData getPersonalData() {
		return personalData;
	}

	public void setPersonalData(PersonalData personalData) {
		this.personalData = personalData;
	}

	public LocalDate getBirtDate() {
		return birthDate;
	}

	public void setBirtDate(LocalDate birtDate) {
		this.birthDate = birtDate;
	}

	public Profession getProffesion() {
		return profession;
	}

	public void setProffesion(Profession proffesion) {
		this.profession = proffesion;
	}

	public AddressEntity getAddressEntity() {
		return addressEntity;
	}

	public void setAddressEntity(AddressEntity addressEntity) {
		this.addressEntity = addressEntity;
	}

	public DepartmentEntity getDepartmentEntity() {
		return departmentEntity;
	}

	public void setDepartmentEntity(DepartmentEntity departmentEntity) {
		this.departmentEntity = departmentEntity;
	}

	public List<CarEntity> getAttendCars() {
		return attendCars;
	}

	public void setAttendCars(List<CarEntity> attendCars) {
		this.attendCars = attendCars;
	}
}
