package com.capgemini.dataaccess.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "department")
public class DepartmentEntity extends AbstractEntity {

	@Column(length = 15)
	@NotNull
	private String phoneNumber;
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "id_address")
	private AddressEntity addressEntity;
	@OneToMany(mappedBy = "departmentEntity")
	private List<EmployeeEntity> employes;
	@OneToMany(mappedBy = "departmentFrom", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ContractEntity> contractFrom;
	@OneToMany(mappedBy = "departmentTo", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ContractEntity> contractTo;

	public DepartmentEntity() {
	}

	public DepartmentEntity(String phoneNumber, AddressEntity addressEntity, List<EmployeeEntity> employes) {
		this.phoneNumber = phoneNumber;
		this.addressEntity = addressEntity;
		this.employes = employes;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public AddressEntity getAddressEntity() {
		return addressEntity;
	}

	public void setAddressEntity(AddressEntity addressEntity) {
		this.addressEntity = addressEntity;
	}

	public List<EmployeeEntity> getEmployes() {
		return employes;
	}

	public void setEmployes(List<EmployeeEntity> employes) {
		this.employes = employes;
		this.employes.stream().forEach(em -> em.setDepartmentEntity(this));
	}
}
