package com.capgemini.dataaccess.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.capgemini.dataaccess.embeded.PersonalData;

@Entity
@Table(name = "customer")
public class CustomerEntity extends AbstractEntity {

	@Embedded
	private PersonalData personalData;
	@Column(length = 20)
	@NotNull
	private String creditCardNumber;
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_address")
	private AddressEntity addressEntity;
	@OneToMany(mappedBy = "customerEntity")
	private List<ContractEntity> contract;

	public CustomerEntity() {
	}

	public CustomerEntity(PersonalData personalData, String creditCardNumber, AddressEntity addressEntity,
			List<ContractEntity> contract) {
		this.personalData = personalData;
		this.creditCardNumber = creditCardNumber;
		this.addressEntity = addressEntity;
		this.contract = contract;
	}

	public List<ContractEntity> getContract() {
		return contract;
	}

	public void setContract(List<ContractEntity> contract) {
		this.contract = contract;
	}

	public PersonalData getPersonalData() {
		return personalData;
	}

	public void setPersonalData(PersonalData personalData) {
		this.personalData = personalData;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public AddressEntity getAddressEntity() {
		return addressEntity;
	}

	public void setAddressEntity(AddressEntity addressEntity) {
		this.addressEntity = addressEntity;
	}
}
