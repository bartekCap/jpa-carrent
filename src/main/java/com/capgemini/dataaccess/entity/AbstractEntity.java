package com.capgemini.dataaccess.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AbstractEntity {

	@Id
	@GeneratedValue
	private Long id;
	@Version
	@Column(columnDefinition = "bigint DEFAULT 0")
	private long version;
	private LocalDateTime creationDate;
	private LocalDateTime updateDateTime;

	@PreUpdate
	public void updateDateTime() {
		updateDateTime = LocalDateTime.now();
	}

	@PrePersist
	public void createCreationDate() {
		creationDate = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(LocalDateTime updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
