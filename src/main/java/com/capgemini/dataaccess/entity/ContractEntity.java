package com.capgemini.dataaccess.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "contract")
public class ContractEntity extends AbstractEntity {

	@NotNull
	private LocalDate rentDate;
	private LocalDate returnDate;
	private float cost;

	@ManyToOne
	@JoinColumn(name = "id_department_from")
	private DepartmentEntity departmentFrom;
	@ManyToOne
	@JoinColumn(name = "id_department_to")
	private DepartmentEntity departmentTo;
	@ManyToOne
	@JoinColumn(name = "id_car")
	private CarEntity carEntity;
	@ManyToOne
	@JoinColumn(name = "id_customer")
	private CustomerEntity customerEntity;

	public ContractEntity() {
	}

	public ContractEntity(LocalDate rentDate, LocalDate returnDate, float cost, DepartmentEntity departmentFrom,
			DepartmentEntity departmentTo, CarEntity carEntity, CustomerEntity customerEntity) {
		this.rentDate = rentDate;
		this.departmentFrom = departmentFrom;
		this.departmentTo = departmentTo;
		this.carEntity = carEntity;
		this.customerEntity = customerEntity;
	}

	public LocalDate getRentDate() {
		return rentDate;
	}

	public void setRentDate(LocalDate rentDate) {
		this.rentDate = rentDate;
	}

	public LocalDate getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public DepartmentEntity getDepartmentFrom() {
		return departmentFrom;
	}

	public void setDepartmentFrom(DepartmentEntity departmentFrom) {
		this.departmentFrom = departmentFrom;
	}

	public DepartmentEntity getDepartmentTo() {
		return departmentTo;
	}

	public void setDepartmentTo(DepartmentEntity departmentTo) {
		this.departmentTo = departmentTo;
	}

	public CarEntity getCarEntity() {
		return carEntity;
	}

	public void setCarEntity(CarEntity carEntity) {
		this.carEntity = carEntity;
	}

	public CustomerEntity getCustomerEntity() {
		return customerEntity;
	}

	public void setCustomerEntity(CustomerEntity customerEntity) {
		this.customerEntity = customerEntity;
	}
}