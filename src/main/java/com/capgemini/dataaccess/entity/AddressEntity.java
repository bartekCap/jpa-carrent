package com.capgemini.dataaccess.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "address")
public class AddressEntity extends AbstractEntity {

	@Column(length = 40)
	@NotNull
	private String street;
	@Column(length = 30)
	@NotNull
	private String city;
	@Column(length = 11)
	@NotNull
	private String postCode;

	public AddressEntity() {
	}

	public AddressEntity(String street, String city, String postCode) {
		this.street = street;
		this.city = city;
		this.postCode = postCode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
}
