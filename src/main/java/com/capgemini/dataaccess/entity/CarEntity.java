package com.capgemini.dataaccess.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.capgemini.dataaccess.enums.CarType;

@Entity
@Table(name = "car")
public class CarEntity extends AbstractEntity {

	@Enumerated(EnumType.STRING)
	@NotNull
	private CarType carType;
	@Column(length = 40)
	@NotNull
	private String mark;
	@NotNull
	private Short productionYear;
	@Column(length = 20)
	@NotNull
	private String color;
	@NotNull
	private Short engineCapacity;
	@NotNull
	private Short power;
	@NotNull
	private int mileage;
	@OneToMany(mappedBy = "carEntity", cascade = CascadeType.REMOVE)
	private List<ContractEntity> contracts;
	@ManyToMany(mappedBy = "attendCars", cascade = CascadeType.MERGE)
	private List<EmployeeEntity> attendantEmployees;

	public CarEntity() {
	}

	public CarEntity(CarType carType, String mark, Short productionYear, String color, Short engineCapacity,
			Short power, int mileage) {
		this.carType = carType;
		this.mark = mark;
		this.productionYear = productionYear;
		this.color = color;
		this.engineCapacity = engineCapacity;
		this.power = power;
		this.mileage = mileage;
	}

	public CarEntity(Short power) {
		this.power = power;
	}

	public CarType getCarType() {
		return carType;
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public Short getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(Short productionYear) {
		this.productionYear = productionYear;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Short getEngineCapcity() {
		return engineCapacity;
	}

	public void setEngineCapcity(Short engineCapcity) {
		this.engineCapacity = engineCapcity;
	}

	public Short getPower() {
		return power;
	}

	public void setPower(Short power) {
		this.power = power;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

	public List<ContractEntity> getContracts() {
		return contracts;
	}

	public void setContracts(List<ContractEntity> contracts) {
		this.contracts = contracts;
	}

	public List<EmployeeEntity> getAttendantEmployee() {
		return attendantEmployees;
	}

	public void setAttendantEmployee(List<EmployeeEntity> attendantEmployee) {
		this.attendantEmployees = attendantEmployee;
		this.attendantEmployees.stream().forEach(em -> {
			List<CarEntity> cars = em.getAttendCars();
			cars.add(this);
			em.setAttendCars(cars);
		});
	}
}
