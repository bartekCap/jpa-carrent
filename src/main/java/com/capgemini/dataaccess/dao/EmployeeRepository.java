package com.capgemini.dataaccess.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capgemini.dataaccess.entity.EmployeeEntity;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

}
