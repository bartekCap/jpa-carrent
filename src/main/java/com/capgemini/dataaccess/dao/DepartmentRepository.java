package com.capgemini.dataaccess.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.entity.DepartmentEntity;
import com.capgemini.dataaccess.entity.EmployeeEntity;

public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Long> {

	@Query("SELECT e FROM EmployeeEntity e WHERE e.departmentEntity = :department AND :car MEMBER OF e.attendCars")
	List<EmployeeEntity> findEmployesByDepartmentAndAttendantCar(
			@Param(value = "department") DepartmentEntity department, @Param(value = "car") CarEntity car);

	@Query("SELECT em FROM EmployeeEntity em WHERE em.departmentEntity.id=:id")
	List<EmployeeEntity> findEmployesByDepartmentEntityId(@Param(value = "id") Long id);
}
