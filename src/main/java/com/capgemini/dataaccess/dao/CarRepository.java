package com.capgemini.dataaccess.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.dataaccess.enums.CarType;

public interface CarRepository extends JpaRepository<CarEntity, Long> {

	List<CarEntity> findByCarTypeAndMark(CarType carType, String mark);

	List<CarEntity> findByAttendantEmployees(EmployeeEntity employeeEntity);

	@Query("SELECT ce FROM CarEntity ce JOIN ce.contracts c GROUP BY ce.id HAVING COUNT(DISTINCT c.customerEntity.id)>=10")
	List<CarEntity> findCarsRentedByMoreThenTenCustomers();

	@Query("SELECT COUNT(c) FROM ContractEntity c WHERE c.rentDate BETWEEN :firstDate AND :secondDate")
	int getCountOfCarsRentedBetwenDates(@Param(value = "firstDate") LocalDate firstDate,
			@Param(value = "secondDate") LocalDate secondDate);
}
