package com.capgemini.dataaccess.enums;

public enum Profession {

	MANAGER, ACCOUNTANT, DEALER
}
