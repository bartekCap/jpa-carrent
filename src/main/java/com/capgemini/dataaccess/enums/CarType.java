package com.capgemini.dataaccess.enums;

public enum CarType {

	SEDAN, COMBI, VAN, SPORT, LIMUZINE
}
