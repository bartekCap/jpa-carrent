package com.capgemini.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.dataaccess.dao.DepartmentRepository;
import com.capgemini.dataaccess.dao.EmployeeRepository;
import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.entity.DepartmentEntity;
import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.service.DepartmentService;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public List<EmployeeEntity> getEmployeesFromDepartment(Long idDepartment) {
		return departmentRepository.findEmployesByDepartmentEntityId(idDepartment);
	}

	@Override
	public List<EmployeeEntity> getAttendantsByDepartmentAndCar(CarEntity car, DepartmentEntity department) {
		return departmentRepository.findEmployesByDepartmentAndAttendantCar(department, car);
	}

	@Override
	public DepartmentEntity add(DepartmentEntity departmentEntity) {
		return departmentRepository.save(departmentEntity);
	}

	@Override
	public DepartmentEntity update(DepartmentEntity departmentEntity) {
		return departmentRepository.save(departmentEntity);
	}

	@Override
	public void remove(Long id) {
		DepartmentEntity departmentEntity = departmentRepository.findOne(id);
		departmentEntity.getEmployes().stream().forEach(em -> em.setDepartmentEntity(null));
		departmentRepository.delete(id);
	}

	@Override
	public List<DepartmentEntity> findAll() {
		return departmentRepository.findAll();
	}

	@Override
	public DepartmentEntity findOne(Long id) {
		return departmentRepository.findOne(id);
	}

	@Override
	public void addEmployeeToDepartment(Long idEmployee, Long idDepartment) {
		DepartmentEntity departmentEntity = departmentRepository.findOne(idDepartment);
		EmployeeEntity employeeEntity = employeeRepository.findOne(idEmployee);
		List<EmployeeEntity> employess = departmentEntity.getEmployes();
		employess.add(employeeEntity);
		departmentEntity.setEmployes(employess);
		departmentRepository.save(departmentEntity);
	}

	@Override
	public void removeEmployeeFromDepartment(Long idEmployee, Long idDepartment) {
		EmployeeEntity employeeEntity = employeeRepository.findOne(idEmployee);
		DepartmentEntity departmentEntity = employeeEntity.getDepartmentEntity();
		employeeEntity.setDepartmentEntity(null);
		List<EmployeeEntity> employees = departmentEntity.getEmployes();
		employees.remove(employeeEntity);
		departmentEntity.setEmployes(employees);
		departmentRepository.save(departmentEntity);
	}
}
