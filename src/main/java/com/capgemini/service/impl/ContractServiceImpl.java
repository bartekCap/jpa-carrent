package com.capgemini.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.dataaccess.dao.ContractRepository;
import com.capgemini.dataaccess.entity.ContractEntity;
import com.capgemini.service.ContractService;

@Service
@Transactional
public class ContractServiceImpl implements ContractService {

	@Autowired
	private ContractRepository contractRepository;

	@Override
	public List<ContractEntity> findAll() {
		return contractRepository.findAll();
	}

	@Override
	public ContractEntity findOne(Long id) {
		return contractRepository.findOne(id);
	}
}
