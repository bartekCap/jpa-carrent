package com.capgemini.service.impl;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.dataaccess.dao.CarRepository;
import com.capgemini.dataaccess.dao.EmployeeRepository;
import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.dataaccess.enums.CarType;
import com.capgemini.service.CarService;

@Transactional
@Service
public class CarServiceImpl implements CarService {

	private CarRepository carRepository;
	private EmployeeRepository employeeRepository;

	@Autowired
	public CarServiceImpl(CarRepository carRepository, EmployeeRepository employeeRepository) {
		this.carRepository = carRepository;
		this.employeeRepository = employeeRepository;
	}

	@Override
	public CarEntity add(CarEntity carEntity) {
		return carRepository.save(carEntity);
	}

	@Override
	public CarEntity update(CarEntity carEntity) {
		return carRepository.save(carEntity);
	}

	@Override
	public void removeCarById(Long id) {
		CarEntity car = findOne(id);
		car.getAttendantEmployee().stream().forEach(em -> {
			List<CarEntity> list = em.getAttendCars();
			list.remove(car);
			em.setAttendCars(list);
		});
		carRepository.delete(id);
	}

	@Override
	public List<CarEntity> getCarsByCarTypeAndMark(CarType carType, String mark) {
		return carRepository.findByCarTypeAndMark(carType, mark);
	}

	@Override
	public List<CarEntity> getCarsByAttendantId(Long idEmployee) {
		EmployeeEntity em = new EmployeeEntity();
		em.setId(idEmployee);
		return carRepository.findByAttendantEmployees(em);
	}

	@Override
	public List<CarEntity> getCarsRentedByMoreThenTenCustomers() {
		return carRepository.findCarsRentedByMoreThenTenCustomers();
	}

	@Override
	public CarEntity findOne(Long id) {
		return carRepository.findOne(id);
	}

	@Override
	public List<CarEntity> findAll() {
		return carRepository.findAll();
	}

	@Override
	public void addAttendantToCar(Long idCar, Long idAttendant) {
		CarEntity car = carRepository.findOne(idCar);
		List<EmployeeEntity> employees = car.getAttendantEmployee();
		employees.add(employeeRepository.findOne(idAttendant));
		car.setAttendantEmployee(employees);
		carRepository.save(car);
	}

	@Override
	public int getCountOfCarsRentedBetwenDates(LocalDate firstDate, LocalDate secondDate) {
		return carRepository.getCountOfCarsRentedBetwenDates(firstDate, secondDate);
	}

}
