package com.capgemini.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.dataaccess.dao.EmployeeRepository;
import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public List<EmployeeEntity> findAll() {
		return employeeRepository.findAll();
	}

	@Override
	public EmployeeEntity getEmployee(Long id) {
		return employeeRepository.findOne(id);
	}

	@Override
	public EmployeeEntity findOne(Long id) {
		return employeeRepository.findOne(id);
	}

	@Override
	public EmployeeEntity add(EmployeeEntity employeeEntity) {
		return employeeRepository.save(employeeEntity);
	}

}
