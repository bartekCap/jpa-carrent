package com.capgemini.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Service;

import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.searchcriteria.EmployeeRowMapper;
import com.capgemini.searchcriteria.EmployeeSearchCriteria;
import com.capgemini.service.CriteriaEmployeeService;

@Service
@Transactional
public class CriteriaEmployeeServiceImpl implements CriteriaEmployeeService {

	@Autowired
	private NamedParameterJdbcOperations jdbcTemplate;

	@Autowired
	private EmployeeRowMapper employeeRowMapper;

	private static final String FIND_EMPLOYEE_BY_CRITERIA_SQL = "SELECT em.id, em.first_name, em.surname FROM employee em ";
	private static final String FIND_EMPLOYEE_BY_CAR_JOIN = " JOIN attendant a ON a.id_employee=em.id ";
	private static final String FIND_EMPLOYEE_BY_CAR = " a.id_car=:attendCar ";
	private static final String FIND_EMPLOYEE_BY_DEPARTMENT = " em.id_department=:idDepartment ";
	private static final String FIND_EMPLOYEE_BY_PROFESSION = " em.profession LIKE :profession ";
	private static final String WHERE = " WHERE ";
	private static final String AND = " AND ";
	private static StringBuilder endingQuery;

	@Override
	public List<EmployeeEntity> findEmployeesBySearchCriteria(EmployeeSearchCriteria employeeSearchCriteria) {
		MapSqlParameterSource params = getParamAndInitialEndingQuery(employeeSearchCriteria);
		return jdbcTemplate.query(endingQuery.toString(), params, employeeRowMapper);
	}

	private MapSqlParameterSource getParamAndInitialEndingQuery(EmployeeSearchCriteria employeeSearchCriteria) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		endingQuery = new StringBuilder(FIND_EMPLOYEE_BY_CRITERIA_SQL);

		if (employeeSearchCriteria.getAttendCar() != null) {
			endingQuery.append(FIND_EMPLOYEE_BY_CAR_JOIN + WHERE + FIND_EMPLOYEE_BY_CAR);
			params.addValue("attendCar", employeeSearchCriteria.getAttendCar().getId());
		}

		if (employeeSearchCriteria.getDepartment() != null && employeeSearchCriteria.getAttendCar() != null) {
			endingQuery.append(AND + FIND_EMPLOYEE_BY_DEPARTMENT);
			params.addValue("idDepartment", employeeSearchCriteria.getDepartment().getId());
		} else if (employeeSearchCriteria.getDepartment() != null && employeeSearchCriteria.getAttendCar() == null) {
			endingQuery.append(WHERE + FIND_EMPLOYEE_BY_DEPARTMENT);
			params.addValue("idDepartment", employeeSearchCriteria.getDepartment().getId());
		}

		if (employeeSearchCriteria.getProfession() != null
				&& (employeeSearchCriteria.getAttendCar() != null || employeeSearchCriteria.getDepartment() != null)) {
			endingQuery.append(AND + FIND_EMPLOYEE_BY_PROFESSION);
			params.addValue("profession", employeeSearchCriteria.getProfession().toString());
		} else if (employeeSearchCriteria.getProfession() != null) {
			endingQuery.append(WHERE + FIND_EMPLOYEE_BY_PROFESSION);
			params.addValue("profession", employeeSearchCriteria.getProfession().toString());
		}
		return params;
	}

}
