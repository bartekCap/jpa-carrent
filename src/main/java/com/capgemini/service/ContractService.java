package com.capgemini.service;

import java.util.List;

import com.capgemini.dataaccess.entity.ContractEntity;

public interface ContractService {

	List<ContractEntity> findAll();

	ContractEntity findOne(Long id);

}
