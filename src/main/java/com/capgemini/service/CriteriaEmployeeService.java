package com.capgemini.service;

import java.util.List;

import com.capgemini.dataaccess.entity.EmployeeEntity;
import com.capgemini.searchcriteria.EmployeeSearchCriteria;

public interface CriteriaEmployeeService {

	List<EmployeeEntity> findEmployeesBySearchCriteria(EmployeeSearchCriteria employeeSearchCriteria);

}
