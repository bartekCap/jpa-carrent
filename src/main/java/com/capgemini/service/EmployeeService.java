package com.capgemini.service;

import java.util.List;

import com.capgemini.dataaccess.entity.EmployeeEntity;

public interface EmployeeService {

	EmployeeEntity getEmployee(Long id);

	List<EmployeeEntity> findAll();

	EmployeeEntity findOne(Long id);

	EmployeeEntity add(EmployeeEntity employeeEntity);

}
