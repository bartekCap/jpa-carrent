package com.capgemini.service;

import java.util.List;

import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.entity.DepartmentEntity;
import com.capgemini.dataaccess.entity.EmployeeEntity;

public interface DepartmentService {

	DepartmentEntity add(DepartmentEntity departmentEntity);

	DepartmentEntity update(DepartmentEntity departmentEntity);

	DepartmentEntity findOne(Long id);

	void remove(Long id);

	void addEmployeeToDepartment(Long idEmployee, Long idDepartment);

	void removeEmployeeFromDepartment(Long idEmployee, Long idDepartment);

	List<EmployeeEntity> getEmployeesFromDepartment(Long idDepartment);

	List<DepartmentEntity> findAll();

	List<EmployeeEntity> getAttendantsByDepartmentAndCar(CarEntity car, DepartmentEntity department);
}
