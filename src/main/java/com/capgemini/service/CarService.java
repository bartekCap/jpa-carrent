package com.capgemini.service;

import java.time.LocalDate;
import java.util.List;

import com.capgemini.dataaccess.entity.CarEntity;
import com.capgemini.dataaccess.enums.CarType;

public interface CarService {
	CarEntity add(CarEntity carEntity);

	CarEntity update(CarEntity carEntity);

	CarEntity findOne(Long id);

	List<CarEntity> findAll();

	void addAttendantToCar(Long idCar, Long idAttendant);

	void removeCarById(Long id);

	List<CarEntity> getCarsByCarTypeAndMark(CarType carType, String Mark);

	List<CarEntity> getCarsByAttendantId(Long idEmployee);

	List<CarEntity> getCarsRentedByMoreThenTenCustomers();

	int getCountOfCarsRentedBetwenDates(LocalDate firstDate, LocalDate secondDate);

}
